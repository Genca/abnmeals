export interface Ingredient {
    ingriedient: string;
    measure: string;
}