import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class MealService {

  private API_URL = "https://www.themealdb.com/api/json/v1/1/";

  constructor(private http: HttpClient, private saniter: DomSanitizer) { }

  public getRandomMeal() {
    return this.http.get(this.API_URL + "random.php").pipe(
      map((response) => {

        let meals = response["meals"];
        if (meals.length > 0) {
          return this.formatMeal(meals[0]);
        }
        return undefined;
      })
    );
  }

  public searchForMeal(searchInput: string) {
    const params = new HttpParams().set('s', searchInput);
    return this.http.get(this.API_URL + "search.php", { params }).pipe(
      map((response) => {
        let meals = response["meals"];
        return meals.map(meal => {
          return this.formatMeal(meal);
        });
      })
    );
  }

  private formatMeal(result: any) {

    const ingredients = [];
    for (let i = 0; i < Object.keys(result).length; i++) {
      let ingredient = result["strIngredient" +[i]];
      if (ingredient) {
        ingredients.push({
          ingredient: ingredient,
          measure: result["strMeasure" + i]
        });
      }
    }
    let meal = {
      id: result["idMeal"],
      name: result["strMeal"],
      image: result["strMealThumb"],
      instruction: result["strInstructions"],
      ingredients: ingredients
    }
    return meal;

  }
}
