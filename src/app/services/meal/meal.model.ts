import { Ingredient } from './ingredient.model';
import { SafeResourceUrl } from '@angular/platform-browser';

export interface Meal {
    id: string;
    name: string;
    image: string;
    instruction: string;
    ingredients: Ingredient[];
}

