import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { DetailsComponent } from './components/meal/details/details.component';
import { SearchComponent } from './components/meal/search/search.component';


const routes: Routes = [{
  path: '', component: HomeComponent
},
{
  path: 'meal', children: [
    { path: 'random', component: DetailsComponent },
    { path: 'details', component: DetailsComponent },
    { path: 'search', component: SearchComponent }
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
