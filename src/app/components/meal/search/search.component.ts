import { Component, OnInit } from '@angular/core';
import { MealService } from 'src/app/services/meal/meal.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  private searchInput: string;
  private results = [];

  constructor(private mealService: MealService) { }

  ngOnInit() {
  }

  searchForMeals() {
    if (this.searchInput && this.searchInput !== '') {
      this.mealService.searchForMeal(this.searchInput).subscribe(meals => this.results = meals);
      this.searchInput = '';
    }
  }

}
