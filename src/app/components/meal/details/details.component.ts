import { Component, OnInit } from '@angular/core';
import { MealService } from 'src/app/services/meal/meal.service';
import { Meal } from 'src/app/services/meal/meal.model';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  private meal: Meal;

  constructor(private mealService: MealService, private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    this.activatedRoute.paramMap
      .pipe(map(() => window.history.state)).subscribe(state => {
        if (state && state.name) {
          console.log(state);
          this.meal = state as Meal;
        } else {
          this.loadRandomMeal();
        }
      })
  }

  public loadRandomMeal() {
    this.mealService.getRandomMeal().subscribe(meal => {
      this.meal = meal;
    });
  }

}
