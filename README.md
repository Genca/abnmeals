# ABNMeals

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Architectual choices

The following frameworks are used for this application:

- Angular 8
- Bootstap

A couple of choices are made: 

- To format the API response to new models. This is done because the API response was a very unstructured model. 
- To pass an object from searchcomponent to detailcomponent through a link, I used the state in the routerlink (available since angular 7.2). This allows me to temporarly store and pass the object selected. Downside is that after a refresh the state will be gone, but it was a simple sollution for now. 
- Seperation of services and components for te sake of clarity
